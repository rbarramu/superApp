//
//  productsCollectionViewController.swift
//  superApp
//
//  Created by Rocio Barramuño on 14-02-17.
//  Copyright © 2017 Rocio Barramuño. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class productsCollectionViewController: UICollectionViewController {
    var carImages = [String]()
    var textLabel = [String]()
    var productSelected = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        layout()
        carImages = ["Apple.jpg",
                     "Apricot.jpg",
                     "first.jpg", "sofi.jpg", "sofi-1.jpg", "sofi-2.jpg", "sofi-3.jpg"]
        
        textLabel = ["manzana", "Apricot", "first", "sofi", "sofi1" ,"sofi2", "sofi3"]
        
        
        productSelected = []
        
        // Define identifier
        let notificationName = Notification.Name("NotificationIdentifier")
        
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(productsCollectionViewController.refreshCollection), name: notificationName, object: nil)
        
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

   
    func refreshCollection(){
        collectionView!.reloadData()
    }
    func layout(){
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        var width = UIScreen.main.bounds.width
        var height = UIScreen.main.bounds.height
        
        layout.sectionInset = UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
        width = width - 20
        height = height - 30
        layout.itemSize = CGSize(width: width / 3 - 5, height: height / 3 - 10)
        layout.minimumInteritemSpacing = 0.3
        layout.minimumLineSpacing = 6
        collectionView!.collectionViewLayout = layout
    }

    

    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return  carImages.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)  as! productsCollectionViewCell

    
        cell.labelText.text = textLabel[indexPath.row]
        
        let image = UIImage(named: carImages[indexPath.row])
        cell.imageView.image = image
        
        cell.layer.borderColor = UIColor.white.cgColor
        cell.layer.borderWidth = 1
        cell.layer.cornerRadius = 8
    
        return cell
    }

    
}
