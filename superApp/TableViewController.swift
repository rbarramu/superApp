//
//  TableViewController.swift
//  superApp
//
//  Created by Rocio Barramuño on 07-02-17.
//  Copyright © 2017 Rocio Barramuño. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {

    var listas = ["Sofi", "Supermercado"]
    var newList: String = ""
    var selectedRow:Int = 0
   
    @IBAction func cancelListButton(_ sender: Any) {
    }
  
    @IBAction func addListButton(segue:UIStoryboardSegue) {
        let carDetailVC = segue.source as! addListViewController
        
        print(carDetailVC.listName.text!)
        newList = carDetailVC.listName.text!
        print(newList)
        listas.append(newList)
        tableView.reloadData()
        
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listas.count
    }
    
   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath)
        
        cell.textLabel?.text = listas[indexPath.row]
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //tableView.deselectRow(at: indexPath as IndexPath, animated: true)
       
        let row = indexPath.row
        selectedRow = indexPath.row
        //print(listas[row])
        performSegue(withIdentifier: "tableToCollection", sender: self)
        
    }
    
    
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        if segue.identifier == "tableToCollection"{
            let vc = segue.destination as! CollectionViewController
            let myIndexPath = self.tableView.indexPathForSelectedRow!.row
            print(myIndexPath)
            
            vc.listForRow = listas[myIndexPath]
            
            
        }
        
    }
    /*
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let cell = sender as? UITableViewCell {
            let i = tableView.indexPath(for: cell)!.row
            if segue.identifier == "tableToCollection" {
                let vc = segue.destination as! CollectionViewController
                vc.data = listForRow[i] as NSDictionary
            }
        }
    }*/

    
  //  func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
   //     return true
    //}
  /*  override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            
            // remove the item from the data model
            listas.remove(at: indexPath.row)
            
            // delete the table view row
            tableView.deleteRows(at: [indexPath], with: .fade)
            
        } else if editingStyle == .insert {
            // Not used in our example, but if you were adding a new row, this is where you would do it.
        }
    }
   
    override func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Borrar"
    }
    */
    
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
               // action two
        let deleteAction = UITableViewRowAction(style: .default, title: "Borrar", handler: { (action, indexPath) in
            print("Delete tapped")
            // remove the item from the data model
            self.listas.remove(at: indexPath.row)
            
            // delete the table view row
            tableView.deleteRows(at: [indexPath], with: .fade)
        })
        deleteAction.backgroundColor = UIColor.red
        
        return [deleteAction]
    }
 
    
}
