//
//  CollectionViewController.swift
//  superApp
//
//  Created by Rocio Barramuño on 13-02-17.
//  Copyright © 2017 Rocio Barramuño. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"



class CollectionViewController: UICollectionViewController {


    var listForRow: String?
    var carImages = [String]()
    var textLabel = [String]()
    var number = [Int]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        configNav()
       
        carImages = ["Apple.jpg",
                     "Apricot.jpg",
                     "first.jpg", "sofi.jpg", "sofi-1.jpg", "sofi-2.jpg", "sofi-3.jpg"]

        textLabel = ["manzana", "Apricot", "first", "sofi", "sofi1" ,"sofi2", "sofi3"]
      
        number = [1, 1, 1 , 1 ,1 ,1, 1]
        layout()
        
      
    }

    func layout(){
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        var width = UIScreen.main.bounds.width
        var height = UIScreen.main.bounds.height
        
        layout.sectionInset = UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
        width = width - 10
        height = height - 10
        layout.itemSize = CGSize(width: width / 2 - 5, height: height / 3 + 20)
        layout.minimumInteritemSpacing = 0.3
        layout.minimumLineSpacing = 10
        collectionView!.collectionViewLayout = layout
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func configNav(){
 
    let rightAddButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(CollectionViewController.addProduct))
    let rightDeleteButtonItem = UIBarButtonItem(title: "Borrar", style: .plain, target: self, action: #selector(EditAlbumPressed))
  
    
    self.navigationItem.setRightBarButtonItems([rightAddButtonItem,rightDeleteButtonItem], animated: true)
    
 
}

    func addProduct(){
    
    }
    func EditAlbumPressed() {
        
        if(self.navigationItem.rightBarButtonItems?[1].title == "Borrar"){
            
            self.navigationItem.rightBarButtonItems?[1].title = "Listo"
            
            
            for item in self.collectionView!.visibleCells as! [CollectionViewCell] {
                
                let indexpath : NSIndexPath = self.collectionView!.indexPath(for: item as CollectionViewCell)! as NSIndexPath
                let cell : CollectionViewCell = self.collectionView!.cellForItem(at: indexpath as IndexPath) as! CollectionViewCell
                
                //Profile Picture
                //var img : UIImageView = cell.viewWithTag(100) as UIImageView
                //img.image = UIImage(named: "q.png") as UIImage
                
                //Close Button
                let close : UIButton = cell.viewWithTag(102) as! UIButton
                close.isHidden = false
            }
        } else {
            self.navigationItem.rightBarButtonItems?[1].title = "Borrar"
            self.collectionView?.reloadData()
        }
    }

  
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return carImages.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)  as! CollectionViewCell
      
       // cell.listLabel.text = listForRow
        // Configure the cell
      
        cell.listLabel.text = textLabel[indexPath.row]
        cell.backgroundView = UIImageView(image: UIImage(named: "photo-frame.png")) as UIView
        let image = UIImage(named: carImages[indexPath.row])
        cell.imageView.image = image
        
        cell.layer.borderColor = UIColor.white.cgColor
        cell.layer.borderWidth = 1
        cell.layer.cornerRadius = 8
        
        cell.numberIncrement.text = String(number[indexPath.row])
        
        
       // let right = UISwipeGestureRecognizer(target: self, action: #selector(test))
       // right.direction = UISwipeGestureRecognizerDirection.left
       // cell.addGestureRecognizer(right)
        
        if self.navigationItem.rightBarButtonItems?[1].title == "Borrar" {
            cell.closeImage?.isHidden = true
        } else {
            cell.closeImage?.isHidden = false
        }
        cell.closeImage?.layer.setValue(indexPath.row, forKey: "index")
        cell.closeImage?.addTarget(self, action: #selector(CollectionViewController.deletePhoto), for: UIControlEvents.touchUpInside)

     
        cell.plusIncrement?.addTarget(self, action: #selector(CollectionViewController.plusNumber), for: UIControlEvents.touchUpInside)
        cell.plusIncrement.tag = indexPath.row
        
       
        cell.subIncrement?.addTarget(self, action: #selector(CollectionViewController.subNumber), for: UIControlEvents.touchUpInside)
        cell.subIncrement.tag = indexPath.row
        
        return cell
    }

    func plusNumber(_ sender:UIButton){
        number[sender.tag] = number[sender.tag] + 1
        collectionView?.reloadData()
    }
    
    func subNumber(_ sender:UIButton){
        number[sender.tag] = number[sender.tag] - 1
        collectionView?.reloadData()
    }
    
    func deletePhoto(_ sender:UIButton){
        let i : Int = (sender.layer.value(forKey: "index")) as! Int
        carImages.remove(at: i)
        self.collectionView!.reloadData()

    }
    


}
