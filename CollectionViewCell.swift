//
//  CollectionViewCell.swift
//  superApp
//
//  Created by Rocio Barramuño on 13-02-17.
//  Copyright © 2017 Rocio Barramuño. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var listLabel: UILabel!
    
    @IBOutlet weak var closeImage: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var numberIncrement: UILabel!
    
    @IBOutlet weak var plusIncrement: UIButton!
    @IBOutlet weak var subIncrement: UIButton!
}
