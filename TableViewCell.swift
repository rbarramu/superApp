//
//  TableViewCell.swift
//  superApp
//
//  Created by Rocio Barramuño on 10-02-17.
//  Copyright © 2017 Rocio Barramuño. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
