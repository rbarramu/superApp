//
//  productsCollectionViewCell.swift
//  superApp
//
//  Created by Rocio Barramuño on 14-02-17.
//  Copyright © 2017 Rocio Barramuño. All rights reserved.
//

import UIKit

class productsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var selectProduct: UIButton!
    @IBOutlet weak var labelText: UILabel!
    @IBOutlet weak var imageView: UIImageView!
}
