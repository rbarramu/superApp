//
//  addListViewController.swift
//  superApp
//
//  Created by Rocio Barramuño on 10-02-17.
//  Copyright © 2017 Rocio Barramuño. All rights reserved.
//

import UIKit
import DatePickerDialog


private let reuseIdentifier = "Cell"

class addListViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var textField: UITextField!
 
    @IBAction func datePickerTapped(_ sender: Any) {
        DatePickerDialog().show("Fecha", doneButtonTitle: "Agregar Fecha", cancelButtonTitle: "Cancelar", datePickerMode: .date) {
            (date) in
            if let dt = date {
                print (dt)
            
                let calendar = Calendar.current
                let month = calendar.component(.month, from: dt)
                let day = calendar.component(.day, from: dt)
                let year = calendar.component(.year, from: dt)
                print("hours = \(day):\(month):\(year)")
                
                
            
                self.textField.text = "\(day)-\(month)-\(year)"
            }

           
        }
    }
    @IBOutlet weak var listName: UITextField!
    var carImages = ["manzana.jpg",
                     "platano.jpg",
                     "sandia.jpg", "limon.jpg", "kiwi.jpg", "melon.jpg", "naranja.jpg"]
    
    var textLabel =  ["Manzana Verde", "Plátano", "Sandía", "Limón", "Kiwi" ,"Melón tuna", "naranja"]

    var productSelected = [String]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configNavigationBar()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func configNavigationBar(){
        
        
        self.navigationItem.setLeftBarButton(UIBarButtonItem(title: "Cancelar", style: .plain, target: self, action: #selector(cancelButton)), animated: true)
        self.navigationItem.setRightBarButton(UIBarButtonItem(title: "Guardar", style: .plain, target: self, action: #selector(SaveButtonBack)), animated: true)

       
        self.navigationItem.title = "Añadir Lista"
       
    }
    
    func cancelButton(){
        self.navigationController?.popToRootViewController(animated: true)

    }

    
    func SaveButtonBack(){
     performSegue(withIdentifier: "addListButton", sender: self)
    }


     func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return  carImages.count
    }
    
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)  as! productsCollectionViewCell
        
        
        cell.labelText.text = textLabel[indexPath.row]
        let image = UIImage(named: carImages[indexPath.row])
        cell.imageView.image = image
        
        cell.layer.borderColor = UIColor.white.cgColor
        cell.layer.borderWidth = 1
        cell.layer.cornerRadius = 8
        
        
        
        cell.selectProduct?.layer.setValue(cell.labelText.text, forKey: "text")

        cell.selectProduct?.layer.setValue(indexPath.row, forKey: "index")
        cell.selectProduct?.addTarget(self, action: #selector(addListViewController.addButton), for: UIControlEvents.touchUpInside)
        
      


        return cell
    }
    

    func addButton(_ sender:UIButton){
        let i : Int = (sender.layer.value(forKey: "index")) as! Int
        let t : String = (sender.layer.value(forKey: "text")) as! String
        print(t)
        print (i)
        
        //print (sender.titleLabel?.text)
        let p : String = (sender.titleLabel?.text)!
        print (p)
        if p == "+ AÑADIR"{
            productSelected.append(t)
            sender.backgroundColor = UIColor.red
            sender.setTitle("- QUITAR", for: .normal)
            print(productSelected)
        }
        if p == "- QUITAR"{
            print(i)
            print (t)
        

            if let index = productSelected.index(of: t) {
                productSelected.remove(at: index)
            }
            
            let greencolor = UIColor(red: 109.0 / 255.0, green: 186.0 / 255.0, blue: 162.0 / 255.0, alpha: 1.0)
            
            sender.backgroundColor = greencolor
            sender.setTitle("+ AÑADIR", for: .normal)
            print(productSelected)
       
        }
        
    
        //let notificationName = Notification.Name("NotificationIdentifier")

      //  NotificationCenter.default.post(name: notificationName, object: nil)

  //  productsCollectionViewController.collectionView!.reloadData()
    //    productsCollectionViewController.reloadData()
    }


}
